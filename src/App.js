import LoginForm from "./Login_form/LoginForm";
import './index.css'
import swal from 'sweetalert';
import { useState, useEffect } from 'react';
import { Route, Routes, useNavigate } from "react-router-dom";
import SignupForm from "./Signup_form/SignupForm";
import Dashboard from "./Dashboard/Dashboard";
import api from "./Api/apiCall";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';



function App() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [rememberMe, setRememberMe] = useState(false);
  const [securityToken, setSecurityToken] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const [IsLoggedIn, setIsLoggedIn] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    // Check if user data is stored in localStorage and load it
    const storedData = localStorage.getItem('userData');
    if (storedData) {
      const { email, password, securityToken } = JSON.parse(storedData);
      setEmail(email);
      setPassword(password);
      setSecurityToken(securityToken);
      setRememberMe(true);
    }
  }, []);

  const generateSecurityToken = () => {
    // Generate a security token with random characters
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const tokenLength = 10;
    let token = '';
    for (let i = 0; i < tokenLength; i++) {
      const randomIndex = Math.floor(Math.random() * characters.length);
      token += characters.charAt(randomIndex);
    }
    return token;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log('called');
    try {
      const response = await api.get('/users');
      const list = response.data;
      const userGot = list.find(user_item => user_item.__email === email && user_item.password === password);
      if (email === "" || password === "") {
        toast.warning("Oops fields can`t empty!",
        {position: toast.POSITION.BOTTOM_CENTER,autoClose: 1000});
      } else if (userGot) {
        if (rememberMe) {
          if (!securityToken) {
            // Check if security token is not set
            const newSecurityToken = generateSecurityToken();
            setSecurityToken(newSecurityToken);
          }
          swal({
            title: 'Success ',
            text: 'Login success!',
            icon: 'success',
            button: 'OK',
          }).then(() => {
            setIsLoggedIn(prevState => !prevState); // Change the state of isLoggedIn to true
          });
          navigate('/Dashboard')
        } else {
          if (!securityToken) {
            // Check if security token is not set
            const newSecurityToken = generateSecurityToken();
            setSecurityToken(newSecurityToken);
          }
          swal({
            title: 'Success ',
            text: 'Login success!',
            icon: 'success',
            button: 'OK',
          }).then(() => {
            setIsLoggedIn(prevState => !prevState); // Change the state of isLoggedIn to true
            setEmail('');
            setPassword('');
          });
        }
      } else {
        toast.warning("Ouch Username or password doesn`t match!",
        {position: toast.POSITION.BOTTOM_CENTER,autoClose: 1000});
        setEmail(email);
        setPassword('');
        setSecurityToken(securityToken);
      }

      // Store or remove user data based on rememberMe checkbox
      if (rememberMe === true) {
        const userData = { email, password, securityToken };
        localStorage.setItem('userData', JSON.stringify(userData));
      } else {
        localStorage.removeItem('userData');
      }
    } catch (error) {
      console.log(error);
    }


  }

  const togglePasswordVisibility = () => {
    // Toggle password visibility
    setShowPassword(!showPassword);
  };

  const handleForgetPassword = (e) => {
    e.preventDefault();

    swal({
      title: 'Reset Password',
      text: 'Please enter your email:',
      content: {
        element: 'input',
        attributes: {
          type: 'email',
          placeholder: 'Email address',
        },
      },
      buttons: ['Cancel', 'Send Reset Link'],
    }).then(async (value) => {
      if (value) {
        // Perform validation on the email
        try {
          const response = await api.get('/users');
          const emailList = response.data;
          const resetEmail = emailList.find(user_item => user_item.__email === value);
          if (resetEmail) {
            // Display success message
            swal({
              title: 'Success',
              text: 'Reset link has been sent to ' + value,
              icon: 'success',
              button: 'OK',
            });
          } else {
            // Display error message for invalid email
            swal({
              title: 'Error',
              text: 'Please enter a valid email',
              icon: 'error',
              button: 'OK',
            });
          }
        } catch (err) {
          console.log(err);
        }
      }
    });
  };

  const handleSignup = (e) => {
    e.preventDefault();
    
  };
  return (

    <div className="App">

      <Routes>
        <Route path="/" element={<LoginForm
          email={email}
          setEmail={setEmail}
          password={password}
          setPassword={setPassword}
          showPassword={showPassword}
          togglePasswordVisibility={togglePasswordVisibility}
          rememberMe={rememberMe}
          setRememberMe={setRememberMe}
          handleForgetPassword={handleForgetPassword}
          handleSignup={handleSignup}
          handleSubmit={handleSubmit}
          
        />} />

        <Route path="/signup" element={<SignupForm />} />

        <Route path="/Dashboard" element={<Dashboard
          IsLoggedIn={IsLoggedIn}
        />} />
      </Routes>

    </div>
  );
}

export default App;
