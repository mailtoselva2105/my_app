import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope, faLock } from "@fortawesome/free-solid-svg-icons";
import "bootstrap/dist/css/bootstrap.min.css";
import "./Signup.css";

const SignupForm = ({ IsLoggedIn }) => {
  return (
    <div className="container-fluid d-flex justify-content-center align-items-center vh-100">
      <div className="card">
        <h2 className="card-header text-center">Sign up</h2>
        <div className="card-body">
          <form>
            <div className="form-group">
              <label htmlFor="s-email">Email</label>
              <div className="input-group">
                <div className="input-group-prepend">
                  <span className="input-group-text pad-1 pad-1" style={{ borderRadius: "0.25rem 0rem 0rem 0.25rem" }}>
                    <FontAwesomeIcon icon={faEnvelope} />
                  </span>
                </div>
                <input
                  type="email"
                  className="form-control"
                  placeholder="Enter your e-mail"
                  id="s-email"
                />
              </div>
            </div>

            <div className="form-group">
              <label htmlFor="s-password">Password</label>
              <div className="input-group">
                <div className="input-group-prepend">
                  <span className="input-group-text pad-1" style={{ borderRadius: "0.25rem 0rem 0rem 0.25rem" }} >
                    <FontAwesomeIcon icon={faLock} />
                  </span>
                </div>
                <input
                  type="password"
                  name="s-password"
                  id="s-password"
                  className="form-control"
                  placeholder="Enter your password"
                />
              </div>
            </div>

            <div className="form-group">
              <label htmlFor="sc-password">Confirm password</label>
              <div className="input-group">
                <div className="input-group-prepend">
                  <span className="input-group-text pad-1" style={{ borderRadius: "0.25rem 0rem 0rem 0.25rem" }} >
                    <FontAwesomeIcon icon={faLock} />
                  </span>
                </div>
                <input
                  type="password"
                  name="sc-password"
                  id="sc-password"
                  className="form-control"
                  placeholder="Enter your confirm password"
                />
              </div>
            </div>

            <div className="form-group">
              <label htmlFor="s-number">Phone number</label>
              <div className="input-group">
                <div className="input-group-prepend">
                  <select
                    name="country-code"
                    id="country-code"
                    className="form-control"
                    style={{ borderRadius: "0.25rem 0rem 0rem 0.25rem" }}
                  >
                    <option value="+91">+91</option>
                    {/* Add other country options here if needed */}
                  </select>
                </div>
                <input
                  type="text"
                  name="s-number"
                  id="s-number"
                  className="form-control"
                  placeholder="Enter your phone number"
                />
              </div>
            </div>

            <div className="d-flex justify-content-center">
              <button type="submit" className="btn btn-primary w-50 mt-3 mb-4">
                Sign up
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default SignupForm;