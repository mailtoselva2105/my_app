import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import { ToastContainer } from "react-toastify";
import "../Login_form/Login.css";
import { Link } from "react-router-dom";


const LoginForm = ({
  email,
  setEmail,
  setPassword,
  password,
  showPassword,
  togglePasswordVisibility,
  setRememberMe,
  rememberMe,
  handleSubmit,
  handleSignup,
  handleForgetPassword,
}) => {
  return (
    <div className="container-fluid d-flex justify-content-center align-items-center vh-100 login-bg">
      <div className="card">
        <h2 className="card-header text-center">Login</h2>
        <div className="card-body">
          <form>
            <div className="form-group">
              <label htmlFor="email">Email</label>
              <input
                type="email"
                className="form-control mt-2"
                id="email"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                autoComplete="email"
              />
            </div>
            <div className="form-group position-relative">
              <label htmlFor="password">Password</label>
              <button
                className="forget-password position-absolute top-0 end-0"
                onClick={handleForgetPassword}
              >
                Forgot password?
              </button>
              <div className="input-group mt-2">
                <input
                  type={showPassword ? "text" : "password"}
                  className="form-control"
                  id="password"
                  placeholder="Enter password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  autoComplete="current-password"
                />
                <button
                  type="button"
                  className="btn btn-outline-secondary"
                  onClick={togglePasswordVisibility}
                >
                  <FontAwesomeIcon icon={showPassword ? faEyeSlash : faEye} />
                </button>
              </div>
            </div>
            <div className="form-group form-check">
              <input
                type="checkbox"
                className="form-check-input"
                id="rememberMe"
                checked={rememberMe}
                onChange={(e) => setRememberMe(e.target.checked)}
              />
              <label className="form-check-label" htmlFor="rememberMe">
                Remember me
              </label>
            </div>
            <div className="d-flex justify-content-between">
              <button
                type="submit"
                className="btn btn-primary my-btn"
                onClick={handleSubmit}
              >
                Login
              </button>

              <button type="submit" className="btn btn-primary my-btn">
                <Link to="/signup" className="text-decoration-none text-light">Sign up</Link>
              </button>
            </div>
            
          </form>
        </div>
      </div>
      <ToastContainer />
    </div>
  );
};

export default LoginForm;
